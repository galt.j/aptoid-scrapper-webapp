#!/usr/bin/env bash

export FLASK_APP=webapp
export FLASK_ENV=development
export APP_CONFIG_FILE=../config/development.py

pipenv run flask run
