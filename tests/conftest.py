"""Fixtures to test webapp"""

import pytest
from webapp import create_app


@pytest.fixture
def app():
    app = create_app({
        'TESTING'   : True,  # noqa E203
        'SECRET_KEY': "test",
    })
    return app


@pytest.fixture
def client(app):

    """
    Args:
        app:
    """
    return app.test_client()
