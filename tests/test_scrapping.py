import os
from typing import Sequence, Tuple, Collection, Union

import pytest

from scrapper.scrapping import parse_page, get_page

T = Sequence[Tuple[str, Sequence[Tuple[str, str]]]]

dir_path = os.path.dirname(os.path.realpath(__file__))

TEST_APPS: T = (
    ("facebook.html", (
        ('Name', 'Facebook'),
        ('Downloads', '50M - 250M'),
        ('Version', '216.0.0.38.104'),
        ('Release date', '2019-04-09 02:06:04')
    )),
    ("instagram.html", (
        ('Name', 'Instagram'),
        ('Downloads', '50M - 250M'),
        ('Version', '90.0.0.0.12'),
        ('Release date', '2019-04-08 22:55:31')
    )),
)

TESTS_PARSING: Collection[T] = (
    *TEST_APPS,
)


@pytest.mark.parametrize("file, expected", TESTS_PARSING)
def test_parse_page(file: str, expected: Sequence[Tuple[str, str]]):
    """
    Here we test parsing of pages to retrieve data
    Pages are loaded from files to avoid data modification on website
    I choose not to test description as the text is long and would ruin the test readability
    """

    with open(f"{dir_path}/{file}", "rb") as f:
        content = f.read()

    parsed = parse_page(content)

    assert next(parsed) == expected[0]
    assert next(parsed) == expected[1]
    assert next(parsed) == expected[2]
    assert next(parsed) == expected[3]


with open(f"{dir_path}/body.html", "rb") as f:
    body = f.read()

T1 = Sequence[Tuple[str, Tuple[bool, Union[str, bytes]]]]

TEST_STATUS: T1 = (
    ('http://httpbin.org/status/404', (False, "App Not Found")),
    ('http://httpbin.org/status/4O0', (False, "Wrong Status Code 400")),
)

TEST_ERRORS: T1 = (
    ('http://httpbin.org/delay/5', (False, "Can't access the website: ConnectionError")),
    ('http://httpbin.org/status/500', (False, "Can't access the website: RetryError")),
    ('thisisnotanurl', (False, "Something went wrong MissingSchema")),

)

TEST_VALID: T1 = (
    ('https://httpbin.org/html', (True, body)),
)

TESTS_GET: Collection[T1] = (
    *TEST_STATUS,
    *TEST_ERRORS,
    *TEST_VALID
)


@pytest.mark.parametrize("url, expected", TESTS_GET)
def test_get_page(url: str, expected: Tuple[bool, Union[str, bytes]]):
    """
    Tests `get_page` and its return.
    """
    resp = get_page(url, timeout=3)
    assert resp == expected
