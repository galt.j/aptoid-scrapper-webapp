import pytest
from typing import Sequence, Tuple
from flask import g

T = Sequence[Tuple[bool, str, bytes, int]]

TEST_FORM: T = (
    (True, "https://google.fr/", b"Invalid UR", 200),
    (True, "https://notavalidapp.fr.aptoide.com/", b"App Not Found", 200),
    (True, "https://candy-crush-saga.fr.aptoide.com/", b"Description", 200),
    (False, "https://candy-crush-saga.fr.aptoide.com/", b"Bad Request", 400),
)


def test_index(client):
    """
    Args:
        client:
    """
    response = client.get('/')
    assert b"Get App Info" in response.data


def test_404(client):
    """
    Args:
        client:
    """
    assert client.get('/not_a_page').status_code == 404


@pytest.mark.parametrize("use_csrf, url, indata, status_code", TEST_FORM)
def test_form(app, use_csrf, url, indata, status_code):
    """
    Args:
        app:
        use_csrf:
        url:
        indata:
        status_code:
    """
    with app.test_client() as c:
        c.get('/')
        csrf_token = g.csrf_token if use_csrf else ""
        resp = c.post('/', follow_redirects=True, data={"url": url, "csrf_token": csrf_token})
        assert resp.status_code == status_code
        assert indata in resp.data
