import pytest
from typing import Sequence, Tuple, Collection

from scrapper.request_utils import requests_retry_session

T = Sequence[Tuple[str, str]]

TEST_STATUS: T = (
    ('http://httpbin.org/status/404', "404"),
    ('http://httpbin.org/status/200', "200"),
)

TEST_ERRORS: T = (
    ('http://httpbin.org/status/500', "RetryError"),
    ('http://httpbin.org/delay/5', "ConnectionError"),
)

TESTS: Collection[T] = (
    *TEST_STATUS,
    *TEST_ERRORS,
)


@pytest.mark.parametrize("url, expected", TESTS)
def test_requests_retry_session(url: str, expected: str) -> None:
    """Tests requests error/status_code using httpbin

    Args:
        url (str):
        expected (str):
    """

    res = None
    try:
        response = requests_retry_session().get(
                url,
                timeout=3
        )
    except Exception as x:
        res = x.__class__.__name__
    else:
        res = str(response.status_code)
    finally:
        assert res == expected
