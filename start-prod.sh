#!/usr/bin/env bash

export PIPENV_VERBOSITY=-1

export FLASK_APP=webapp
export FLASK_ENV=production
export APP_CONFIG_FILE=../config/production.py

pipenv run gunicorn -w 4 -k gevent -b 127.0.0.1:5000 webapp:app
