#!/usr/bin/env bash

pipenv install --dev
pipenv check
pipenv run flake8
pipenv run coverage run --source=. -m pytest
pipenv run coverage report -m
