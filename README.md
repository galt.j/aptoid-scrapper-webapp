# Aptoid Scrapper WebApp

This is a WebApp that allows to get info on _aptoid_ apps like:
   - name
   - numbers of downloads
   - version
   - release date
   - description

It scraps aptoid website to provide this information

---
## Installation

#### Requirements

- python >=3.6
- pipenv

#### Install (Main packages)

These packages are required for running the project

* **requests**:         The *famous* requests package ;)
* **bs4**:              A simple html parser, Scrapy was overkill for this
* **flask**:            I know Alban doesn't like flask, me neither but it was convenient for the exercise
* **flask-wtf**:        Handles forms, csrf protection and fields validation
* **flask-bootstrap**:  Makes things prettier, lazily
* **gunicorn**:         WSGI to run the app
* **gevent**:           Async workers for gunicorn

To install main packages only use

    $ pipenv install


#### Install (Dev Packages)

These packages are required for testing and doc generation

* **pytest**:       Test suite
* **flake8**:       To see code cleanness
* **sphinx**:       To generate this doc :)
* **coverage**      To get code coverage

To install main + dev packages use

    $ pipenv install --dev

##### Generate Doc

    $ cd docs
    $ make html


## Configuration


In order to generate csrf tokens we need a secret key.
Obviously this can't be commited so you have to generate one before running the app


    $ mkdir instance
    $ python -c 'import os; import binascii; print(binascii.hexlify(os.urandom(24)))'
    b'ebe1d1f2910fc678adc246045eaff25e55ee1ea0610999b0'

Put your secret key in  ``instance/config.py`` as following

    SECRET_KEY = b'ebe1d1f2910fc678adc246045eaff25e55ee1ea0610999b0'

**Do not reveal the secret key when committing code.**

## Basic Usage

Run The App

### Run Flask integrated web server (Dev only)


you can run

    $ ./start_dev.sh

which basically sets development environment
variables and runs Flask web server in debug mode



## Run Gunicorn production server

In production we would use an HTTP proxy like Nginx to limit DOS attacks

For the exercise I skipped this and run the app in Gunicorn

To do so you can run


    $ ./start_prod.sh

which basically sets production environment
variables and runs Gunicorn with 4 gevent workers



## Run tests, flake8 and coverage


you can run

    $ ./tests.sh

which basically do

    $ pipenv install --dev
    $ pipenv check
    $ pipenv run flake8
    $ pipenv run coverage run --source=. -m pytest
    $ pipenv run coverage report -m


## Usage


Open ``http://127.0.0.1:5000/`` in your favorite browser fill the form with an aptoide app url like ``https://facebook.en.aptoide.com/`` click the go button.
