"""Here WTF Forms definition"""

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Regexp

# Regex to validate app url on aptoide
_URL_REGEX = r"""^https:\/\/[-a-zA-Z0-9]{1,256}\.[a-z]{2}\.aptoide\.com\/?$"""


class AptoidForm(FlaskForm):
    """WTF form to get the url we want to scrap, validates the url."""
    url = StringField(validators=[DataRequired(), Regexp(regex=_URL_REGEX, message="Invalid URL")])
    submit = SubmitField('Go')
