import os

from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_wtf.csrf import CSRFProtect
from webapp import views


# noinspection PyUnusedLocal
def page_not_found(e):
    """
    Args:
        e:
    """
    return render_template('404.html'), 404


def create_app(test_config=None):
    # create and configure the app
    # noinspection PyShadowingNames
    """
    Args:
        test_config:
    """
    app = Flask(__name__, instance_relative_config=True)

    # Load the default configuration
    app.config.from_object('config.default')

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # Load the file specified by the APP_CONFIG_FILE environment variable
    # Variables defined here will override those in the default configuration
    if os.environ.get("APP_CONFIG_FILE"):
        app.config.from_envvar('APP_CONFIG_FILE')  # pragma: no cover

    # Register custom 404 page
    app.register_error_handler(404, page_not_found)

    # Bootstraps the app.
    # noinspection PyUnusedLocal
    bootstrap = Bootstrap(app)  # noqa F841

    # Csrf protection app wide.
    # noinspection PyUnusedLocal
    csrf = CSRFProtect(app)  # noqa F841

    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    # register the blueprint for views
    app.register_blueprint(views.simple_page)

    return app


app = create_app()
