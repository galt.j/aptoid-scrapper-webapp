"""Here are the views for our webapp"""
from flask import Blueprint
from flask import render_template, flash

from scrapper.scrapping import get_page, parse_page
from .forms import AptoidForm

simple_page = Blueprint('simple_page', __name__)


@simple_page.route('/', methods=["GET", "POST"])
def index():
    """
    Main view of the webapp

    GET:    Displays the form for the user to enter an aptoide url

    POST:   Validates the form.
            Fetches the aptoide page, parses it and
            populates the template to show app information
    """
    form = AptoidForm()

    if form.validate_on_submit():
        status, resp = get_page(form.url.data)
        if status is True:
            data = parse_page(resp)
            return render_template('details.html', infos=data)
        else:
            form = AptoidForm()
            flash(resp, "error")
    return render_template('index.html', title="Get App Info", form=form)
