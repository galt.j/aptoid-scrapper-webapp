Scrapper
========

The scrapper uses **beautifulsoup** to parse html page fetched with **requests**.
A custom requests.session is used to handle retries on specific errors and timeout.

Scrapping
*********
.. automodule:: scrapper.scrapping
    :members:


Utils
*****
.. automodule:: scrapper.request_utils
    :members: