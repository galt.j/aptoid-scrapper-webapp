Configuration
=============

In order to generate csrf tokens we need a secret key.
Obviously this can't be commited so you have to generate one before running the app


.. code-block:: bash

    $ mkdir instance
    $ python -c 'import os; import binascii; print(binascii.hexlify(os.urandom(24)))'
    b'ebe1d1f2910fc678adc246045eaff25e55ee1ea0610999b0'

Put your secret key in  ``instance/config.py`` as following

``SECRET_KEY = b'ebe1d1f2910fc678adc246045eaff25e55ee1ea0610999b0'``

**Do not reveal the secret key when committing code.**
