Basic Usage
===========

Run The App

Run Flask integrated web server (Dev only)
******************************************

you can run

.. code-block:: bash

    $ ./start_dev.sh

which basically sets development environment
variables and runs Flask web server in debug mode

.. literalinclude:: ../start-dev.sh



Run Gunicorn production server
******************************

In production we would use an HTTP proxy like Nginx to limit DOS attacks

For the exercise I skipped this and run the app in Gunicorn

To do so you can run

.. code-block:: bash

    $ ./start_prod.sh

which basically sets production environment
variables and runs Gunicorn with 4 gevent workers

.. literalinclude:: ../start-prod.sh



Run tests, flake8 and coverage
******************************

you can run

.. code-block:: bash

    $ ./tests.sh

which basically do

.. literalinclude:: ../test.sh


Usage
*****

Open ``http://127.0.0.1:5000/`` in your favorite browser fill the form with an aptoide app url like ``https://facebook.en.aptoide.com/`` click the go button.

