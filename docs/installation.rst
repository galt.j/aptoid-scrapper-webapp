Installation
============

Requirements
************

- python >=3.6
- pipenv

Install (Main packages)
-----------------------
These packages are required for running the project

* **requests**:         The *famous* requests package ;)
* **bs4**:              A simple html parser, Scrapy was overkill for this
* **flask**:            I know Alban doesn't like flask, me neither but it was convenient for the exercise
* **flask-wtf**:        Handles forms, csrf protection and fields validation
* **flask-bootstrap**:  Makes things prettier, lazily
* **gunicorn**:         WSGI to run the app
* **gevent**:           Async workers for gunicorn

To install main packages only use

.. code-block:: bash

    $ pipenv install



Install (Dev Packages)
----------------------

These packages are required for testing and doc generation

* **pytest**:       Test suite
* **flake8**:       To see code cleanness
* **sphinx**:       To generate this doc :)
* **coverage**      To get code coverage

To install main + dev packages use

.. code-block:: bash

    $ pipenv install --dev
