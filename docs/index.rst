.. Aptoid Scrapper WebApp documentation master file, created by
   sphinx-quickstart on Tue Apr  9 22:50:19 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Aptoid Scrapper WebApp's documentation!
==================================================

This is a WebApp that allows to get info on *aptoid* apps like:
   - name
   - numbers of downloads
   - version
   - release date
   - description

It scraps aptoid website to provide this information



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   configuration
   basic_usage
   webapp
   scrapper


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
