""" Here we parse html to retrieve the data from aptoid"""

import bs4
from typing import Tuple, Generator, Union
from requests.exceptions import RetryError, ConnectionError
from scrapper.request_utils import requests_retry_session


def get_page(url: str, timeout: int = 5) -> Tuple[bool, Union[str, bytes]]:
    """
    Try to fetch Page and returns its content.
    If success returns (True, page.content)
    if anything gone wrong it returns (False, message describing the problem)
    """
    try:
        resp = requests_retry_session().get(url, timeout=timeout)
    except (RetryError, ConnectionError) as e:
        message = f"Can't access the website: {e.__class__.__name__}"
    except Exception as e:
        message = f"Something went wrong {e.__class__.__name__}"
    else:
        if resp.status_code == 404:
            message = "App Not Found"
        elif resp.status_code == 200:
            return True, resp.content
        else:
            message = f"Wrong Status Code {resp.status_code}"
    return False, message


def parse_page(content: bytes) -> Generator[Tuple[str, str], None, None]:
    """
    Parses the given html page to grab wanted data. Returns a generator of string tuples
    first element is the key, second the value
    """

    soup = bs4.BeautifulSoup(content, 'html.parser')
    details = soup.find("div", class_="popup__content popup__content--app-info").find_all("tr", class_="app-info__row")

    for detail in details:
        det = detail.find_all("td")

        if det[0].get_text() == "App Name: ":
            yield "Name", det[1].get_text()

        elif det[0].get_text() == "Number of downloads: ":
            yield "Downloads", det[1].get_text()

        elif det[0].get_text() == "Version: ":
            yield "Version", det[1].get_text()

        elif det[0].get_text() == "Release date: ":
            yield "Release date", det[1].get_text()
            # Break here because release date is the last data we want.
            break  # pragma: no cover

    yield "Description", soup.find("p", itemprop="description").get_text()
